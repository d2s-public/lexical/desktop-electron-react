const electron = require("electron");
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const Tray = electron.Tray;
const nativeImage = electron.nativeImage;

const path = require("path");
const url = require("url");
const isDev = require("electron-is-dev");
const ipcMain = electron.ipcMain;

let tray, splashScreenWindow, mainWindow, settingsWindow;

//app.dock.hide();
function createWindow() {
    splashScreenWindow = new BrowserWindow({
        width: 900,
        height: 680,
        show: true,
        frame: false,
        fullscreenable: false,
        skipTaskbar: true,
        hasShadow: false,
        resizable: false,
        resizable: false,
        parent: mainWindow
    });
    splashScreenWindow.loadURL(
        isDev
            ? "http://localhost:3000"
            : `file://${path.join(__dirname, "../build/index.html")}`
    );
    splashScreenWindow.on("close", (e) => {
        e.preventDefault();
        settingsWindow.hide();
    });

    mainWindow = new BrowserWindow({
        width: 900,
        height: 680,
        show: false,
        webPreferences: {
            webSecurity: false,
        },
    });
    mainWindow.loadURL(
        isDev
            ? "http://localhost:3000"
            : `file://${path.join(__dirname, "../build/index.html")}`
    );
    mainWindow.on("closed", () => (mainWindow = null));

    /**
     * Setting Window creation configuration
     */
    settingsWindow = new BrowserWindow({
        width: 600,
        height: 900,
        parent: mainWindow,
        show: false,
        webPreferences: {
            webSecurity: false,
        },
    });

    settingsWindow.loadURL(
        isDev
            ? "http://localhost:3000/settings"
            : `file://${path.join(__dirname, "../build/index.html")}`
    );
    settingsWindow.on("close", (e) => {
        e.preventDefault();
        settingsWindow.hide();
    });
}

const createTray = () => {
    const icon = path.join(__dirname, "assets/logo.png");
    const nImage = nativeImage.createFromPath(icon);

    tray = new Tray(nImage);
    tray.on("click", () =>
        mainWindow.isVisible() ? mainWindow.hide() : mainWindow.show()
    );
};

app.whenReady().then(()=>{createTray()})

app.on("ready", createWindow);

app.on("window-all-closed", () => {
    if (process.platform !== "darwin") {
        app.quit();
    }
});

app.on("activate", () => {
    if (mainWindow === null) {
        createWindow();
    }
});

ipcMain.on("toggle-settings", () => settingsWindow.isVisible() ? settingsWindow.hide() : settingsWindow.show());
