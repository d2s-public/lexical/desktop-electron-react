import logo from './logo.svg';
import './App.css';
// import { Component } from 'react';

// const electron = window.require('electron')
// const ipcRenderer = electron.ipcRenderer
// const Menu = electron

// class App extends Component{}

const electron = window.require('electron')
const Menu = electron.remote.Menu

initMenu = ()=>{
  const menu = Menu.buildFromTemplate([
    {
      label: "file",
      submenu: [
        {
          label: "Settings",
          accelerator: "CmdOrCtrl+P",
          click: ()=> ipcRenderer.send("toggle-settings")
        },
        { type: "separator"},
        {
          label: "Find",
          accelerator: "CmdOrCtrl+W",
        }
      ]
    },
    {
      label: "?",
      submenu: [
        {
          label: "Help",
          accelerator: "F1",
          click: ()=> ipcRenderer.send("toggle-help")
        },
        { type: "separator"},
        {
          label: "Check for Updates",
          accelerator: "CmdOrCtrl+U",
        },
        {
          label: "About",
        }
      ]
    }
  ])
}
function App() {
  initMenu()
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
