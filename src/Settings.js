import logo from "./logo.svg";
import "./Settings.css";

function Settings() {
    axios
        .get("settings.json")
        .then(response => {
            return()
        })
        .catch(error => {
            console.log(errror);
        });
    return (
        <div className="Settings">
            <header className="Settings-header">
                <img src={logo} className="Settings-logo" alt="logo" />
                <p>
                    Edit <code>src/Settings.js</code> and save to reload.
                </p>
                <a
                    className="Settings-link"
                    href="https://reactjs.org"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    Learn React
                </a>
            </header>
        </div>
    );
}

export default Settings;
