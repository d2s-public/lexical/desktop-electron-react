/*
import React from 'react'
import ReactDOM from 'react-dom'
import registerServiceWorker from './registerServiceWorker'
import Router from './Router'
import './index.css'
// import App from './App'

ReactDOM.render(
    <Router />,
    document.getElementById('root')
)
registerServiceWorker()
*/
import React from 'react'
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import App from './App'
import Settings from './Settings'

const Router = ()=>(
    <BrowserRouter>
        <Switch>
            <Route exact path="/" component={App}/>
            <Route exact path="/settings" component={Settings}/>
        </Switch>
    </BrowserRouter>
)

export default Router